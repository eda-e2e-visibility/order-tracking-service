package com.streams.eda.samples.order.tracking.util;


import lombok.Value;
import org.apache.kafka.common.serialization.Serde;

@Value
public class StreamTopic<K, V> {
    private final String name;
    private Serde<K> keySerde;
    private Serde<V> valueSerde;
}
