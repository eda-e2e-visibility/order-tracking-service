package com.streams.eda.samples.order.tracking.processor;

import com.streams.eda.samples.order.tracking.util.StreamTopic;
import com.streams.samples.*;
import lombok.Value;

@Value
public class OrderWorkflowTopologyContext {
    private final StreamTopic<OrderKey, OrderWorkflowStepCompleted> workflowStepCompletedTopic;
    private final StreamTopic<OrderKey, OrderExpired> expiredOrderTopic;
    private final StreamTopic<OrderKey, OrderWorkflowTrackingContainer> orderWorkflowTrackingStoreTopic;
    private final StreamTopic<OrderKey, OrderShipped> shippedOrderTopic;
    private final StreamTopic<InvoiceKey, OrderInvoiced> invoicedOrderTopic;
    private final StreamTopic<OrderKey, OrderCompleted> completedOrderTopic;
    private final StreamTopic<OrderKey, OrderWorkflowStepCompleted> orderWorkflowStepCompletedTopic;

}
