package com.streams.eda.samples.order.tracking.util;

import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import io.streamthoughts.azkarra.api.config.Conf;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.serialization.Serde;

import static java.util.Collections.singletonMap;

public class SerdeUtil {
    public static <T extends SpecificRecord> Serde<T> avroKeySerde(Conf conf) {
        var serdeConfig = singletonMap("schema.registry.url", conf.getString("streams.schema.registry.url"));
        Serde<T> serde = new SpecificAvroSerde<>();
        serde.configure(serdeConfig, true);
        return serde;
    }

    public static <T extends SpecificRecord> Serde<T> avroValueSerde(Conf conf) {
        var serdeConfig = singletonMap("schema.registry.url", conf.getString("streams.schema.registry.url"));
        Serde<T> serde = new SpecificAvroSerde<>();
        serde.configure(serdeConfig, false);
        return serde;
    }
}
