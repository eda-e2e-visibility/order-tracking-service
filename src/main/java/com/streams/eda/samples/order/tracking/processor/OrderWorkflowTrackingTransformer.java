package com.streams.eda.samples.order.tracking.processor;

import com.streams.samples.OrderExpired;
import com.streams.samples.OrderKey;
import com.streams.samples.OrderWorkflowStepCompleted;
import com.streams.samples.OrderWorkflowTrackingContainer;
import io.streamthoughts.azkarra.api.config.Conf;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueStore;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Slf4j
public class OrderWorkflowTrackingTransformer implements Transformer<OrderKey, OrderWorkflowStepCompleted, KeyValue<OrderKey, OrderExpired>> {
    private KeyValueStore<OrderKey, OrderWorkflowTrackingContainer> orderWorkflowStepsStore;
    private ProcessorContext processorContext;
    private final Conf conf;
    private Map<String, Long> workflowStepExpiryPeriod;

    public OrderWorkflowTrackingTransformer(Conf conf) {
        this.conf = conf;

        workflowStepExpiryPeriod = HashMap.ofAll(conf.getConfAsMap())
                .filterKeys(k -> k.startsWith("app.workflowStepExpiryInMS."))
                .mapKeys(k -> k.replaceFirst("app\\.workflowStepExpiryInMS\\.", ""))
                .mapValues(Object::toString)
                .mapValues(Long::valueOf);
    }

    @Override
    public void init(ProcessorContext context) {
        processorContext = context;
        orderWorkflowStepsStore = (KeyValueStore<OrderKey, OrderWorkflowTrackingContainer>) context.getStateStore(conf.getString("topic.orderWorkflowStepsStore"));

        processorContext.schedule(Duration.of(conf.getLong("app.checkExpiryDurationInMs"), ChronoUnit.MILLIS),
                PunctuationType.WALL_CLOCK_TIME, this::expireStuckOrders);
    }

    @Override
    public KeyValue<OrderKey, OrderExpired> transform(OrderKey key, OrderWorkflowStepCompleted value) {
        if (completedOrder(value)) {
            orderWorkflowStepsStore.delete(key);
        } else {

            OrderWorkflowTrackingContainer updatedContainer = updateOrInsertWorkflowStep(key, value);
            orderWorkflowStepsStore.put(key, updatedContainer);
        }

        //Always return nothing as the order will be expired later by the Punctuator when it is stuck in one step for
        //long time
        return null;
    }

    private void expireStuckOrders(long currentTimestamp) {
        var allEntriesIterator = orderWorkflowStepsStore.all();
        while (allEntriesIterator.hasNext()) {
            KeyValue<OrderKey, OrderWorkflowTrackingContainer> entry = allEntriesIterator.next();
            if (isStuckOrder(currentTimestamp, entry)) {
                processorContext.forward(entry.key, toOrderExpiredEvent(currentTimestamp, entry));
                orderWorkflowStepsStore.delete(entry.key);
            }
        }

        allEntriesIterator.close();
        processorContext.commit();
    }

    private boolean isStuckOrder(long currentTimestamp, KeyValue<OrderKey, OrderWorkflowTrackingContainer> entry) {
        var container = entry.value;
        long timeToExpireOrder = container.getTimeOfLastCompletedWorkflowStep() + workflowExpiryPeriodFor(container.getLastCompletedWorkflowStep().getStepName());
        return currentTimestamp >= timeToExpireOrder;
    }

    @NotNull
    private OrderExpired toOrderExpiredEvent(long currentTimestamp, KeyValue<OrderKey, OrderWorkflowTrackingContainer> entry) {
        return new OrderExpired(currentTimestamp, entry.value.getLastCompletedWorkflowStep());
    }

    private OrderWorkflowTrackingContainer updateOrInsertWorkflowStep(OrderKey key, OrderWorkflowStepCompleted value) {
        long currentTimestamp = processorContext.timestamp();
        return Option.of(orderWorkflowStepsStore.get(key))
                .map(v -> {
                    //Only update TimeOfLastCompletedWorkflowStep, orderTime should be the first time we see this OrderKey
                    v.setLastCompletedWorkflowStep(value);
                    v.setTimeOfLastCompletedWorkflowStep(currentTimestamp);
                    return v;
                })
                .getOrElse(() -> new OrderWorkflowTrackingContainer(value, currentTimestamp, currentTimestamp));
    }

    private long workflowExpiryPeriodFor(String workflowStepName) {
        return workflowStepExpiryPeriod.
                getOrElse(workflowStepName, Duration.of(2, ChronoUnit.MINUTES).toMillis());
    }

    private boolean completedOrder(OrderWorkflowStepCompleted lastWorkflowCompletedStep) {
        return lastWorkflowCompletedStep.getStepName().equalsIgnoreCase("order-completed");
    }

    @Override
    public void close() {

    }
}
