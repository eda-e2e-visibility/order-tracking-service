package com.streams.eda.samples.order.tracking.processor;

import com.streams.eda.samples.order.tracking.util.StreamTopic;
import com.streams.samples.*;
import io.streamthoughts.azkarra.api.config.Conf;
import io.streamthoughts.azkarra.api.config.Configurable;
import io.streamthoughts.azkarra.api.streams.TopologyProvider;
import io.vavr.Tuple;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.function.Supplier;

@Slf4j
public class OrderWorkflowStepTopologyProvider implements TopologyProvider, Configurable {
    private Conf conf;
    private final Supplier<OrderWorkflowTrackingTransformer> orderWorkflowTrackingTransformerSupplier;
    private final OrderWorkflowTopologyContext topologyContext;

    public OrderWorkflowStepTopologyProvider(Supplier<OrderWorkflowTrackingTransformer> orderWorkflowTrackingTransformerSupplier,
                                             OrderWorkflowTopologyContext topologyContext) {
        this.orderWorkflowTrackingTransformerSupplier = orderWorkflowTrackingTransformerSupplier;
        this.topologyContext = topologyContext;
    }

    @Override
    public void configure(final Conf conf) {
        this.conf = conf;
    }

    @Override
    public String version() {
        return "1";
    }

    @Override
    public Topology get() {
        StreamsBuilder streamsBuilder = new StreamsBuilder();
        addStateStores(streamsBuilder);

        addWorkflowStepSubTopology(streamsBuilder);
        addCompletedOrderSubTopology(streamsBuilder);

        return streamsBuilder.build();
    }

    private void addWorkflowStepSubTopology(StreamsBuilder streamsBuilder) {
        var orderWorkflowStepCompletedStream = this.streamFor(streamsBuilder, topologyContext.getWorkflowStepCompletedTopic());

        orderWorkflowStepCompletedStream
                .peek((k, v) -> log.info("Received a workflow-step-completed for order with KEY: {}", k))
                .transform(orderWorkflowTrackingTransformerSupplier::get, topologyContext.getOrderWorkflowTrackingStoreTopic().getName())
                .peek((k, v) -> log.info("Order with ID {} is stuck already. It got expired", k))
                .to(topologyContext.getExpiredOrderTopic().getName(),
                        producedWith(topologyContext.getExpiredOrderTopic()));
    }

    private <K, V> Produced<K, V> producedWith(StreamTopic<K, V> topic) {
        return Produced.with(topic.getKeySerde(), topic.getValueSerde());
    }

    public void addCompletedOrderSubTopology(StreamsBuilder streamsBuilder) {
        var orderShippedStream = this.streamFor(streamsBuilder, topologyContext.getShippedOrderTopic());
        var orderInvoicedStream = this.streamFor(streamsBuilder, topologyContext.getInvoicedOrderTopic());
        var expiredOrdersTable = this.tableFor(streamsBuilder, topologyContext.getExpiredOrderTopic());

        var orderCompletedStream = filterOutExpiredOrders(toCompletedOrder(orderShippedStream, orderInvoicedStream), expiredOrdersTable);

        publishWorkflowOperationalEventFor(orderCompletedStream);
        orderCompletedStream.to(topologyContext.getCompletedOrderTopic().getName(),
                Produced.with(topologyContext.getCompletedOrderTopic().getKeySerde(), topologyContext.getCompletedOrderTopic().getValueSerde()));
    }

    private void publishWorkflowOperationalEventFor(KStream<OrderKey, OrderCompleted> orderCompletedStream) {
        orderCompletedStream.mapValues(v -> new OrderWorkflowStepCompleted("order-completed", "order-placed", v.getCompletedTime()))
                .to(conf.getString("topic.orderWorkflowStepCompleted"), producedWith(topologyContext.getWorkflowStepCompletedTopic()));
    }

    private KStream<OrderKey, OrderCompleted> filterOutExpiredOrders(KStream<OrderKey, OrderCompleted> completedStream,
                                                                     KTable<OrderKey, OrderExpired> expiredOrdersTable) {
        Joined<OrderKey, OrderCompleted, OrderExpired> joined = Joined.with(topologyContext.getCompletedOrderTopic().getKeySerde(),
                topologyContext.getCompletedOrderTopic().getValueSerde(), topologyContext.getExpiredOrderTopic().getValueSerde())
                .withName("order-expired-completed-join");

        return completedStream.leftJoin(expiredOrdersTable, Tuple::of, joined)
                .filter((k, v) -> v._2 == null)
                .mapValues(v -> v._1);
    }

    private KStream<OrderKey, OrderCompleted> toCompletedOrder(KStream<OrderKey, OrderShipped> orderShippedStream,
                                                               KStream<InvoiceKey, OrderInvoiced> orderInvoicedStream) {
        StreamJoined<OrderKey, OrderInvoiced, OrderShipped> joined = StreamJoined.with(topologyContext.getShippedOrderTopic().getKeySerde(),
                topologyContext.getInvoicedOrderTopic().getValueSerde(), topologyContext.getShippedOrderTopic().getValueSerde())
                .withName("order-invoiced-shipped-join");

        JoinWindows joinWindow = JoinWindows.of(Duration.of(1, ChronoUnit.HOURS));

        return orderInvoicedStream.selectKey((k, v) -> v.getOrderId())
                .join(orderShippedStream, (v1, v2) -> new OrderCompleted(Instant.now().toEpochMilli()),
                        joinWindow, joined)
                .peek((k, v) -> log.info("Order with ID {} is completed", k));
    }

    private <K, V> KStream<K, V> streamFor(StreamsBuilder streamsBuilder,
                                           StreamTopic<K, V> topic) {
        return streamsBuilder.stream(topic.getName(), Consumed.with(topic.getKeySerde(), topic.getValueSerde()));
    }

    private <K, V> KTable<K, V> tableFor(StreamsBuilder streamsBuilder,
                                         StreamTopic<K, V> topic) {
        return streamsBuilder.table(topic.getName(), Consumed.with(topic.getKeySerde(), topic.getValueSerde()));
    }

    private void addStateStores(StreamsBuilder streamsBuilder) {
        var topic = topologyContext.getOrderWorkflowTrackingStoreTopic();
        final StoreBuilder<KeyValueStore<OrderKey, OrderWorkflowTrackingContainer>> orderWorkflowStepsTrackingStateStore = Stores
                .keyValueStoreBuilder(
                        Stores.persistentKeyValueStore(topic.getName()),
                        topic.getKeySerde(), topic.getValueSerde());

        streamsBuilder.addStateStore(orderWorkflowStepsTrackingStateStore);
    }
}
