package com.streams.eda.samples.order.tracking;

import com.streams.eda.samples.order.tracking.processor.OrderWorkflowStepTopologyProvider;
import com.streams.eda.samples.order.tracking.processor.OrderWorkflowTopologyContext;
import com.streams.eda.samples.order.tracking.processor.OrderWorkflowTrackingTransformer;
import com.streams.eda.samples.order.tracking.util.StreamTopic;
import io.streamthoughts.azkarra.api.AzkarraContext;
import io.streamthoughts.azkarra.api.Executed;
import io.streamthoughts.azkarra.api.StreamsExecutionEnvironment;
import io.streamthoughts.azkarra.api.config.Conf;
import io.streamthoughts.azkarra.api.streams.TopologyProvider;
import io.streamthoughts.azkarra.runtime.context.DefaultAzkarraContext;
import io.streamthoughts.azkarra.runtime.env.DefaultStreamsExecutionEnvironment;
import io.streamthoughts.azkarra.streams.AzkarraApplication;
import io.streamthoughts.azkarra.streams.config.AzkarraConf;
import io.streamthoughts.azkarra.streams.config.HttpServerConf;
import org.apache.avro.specific.SpecificRecord;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

import static com.streams.eda.samples.order.tracking.util.SerdeUtil.avroKeySerde;
import static com.streams.eda.samples.order.tracking.util.SerdeUtil.avroValueSerde;

public class OrderTrackerApp {
    public static void main(String[] args) {
        Conf conf = AzkarraConf.create("application");
        StreamsExecutionEnvironment env = DefaultStreamsExecutionEnvironment.create(conf, DefaultAzkarraContext.DEFAULT_ENV_NAME)
                .setWaitForTopicsToBeCreated(true);

        env.addTopology(orderWorkflowTopologyProvider(conf), Executed.as("OrderWorkflowStepTrackingTopologyProvider"));

        AzkarraContext azkarraContext = DefaultAzkarraContext.create(conf)
                .addExecutionEnvironment(env)
                .setRegisterShutdownHook(true);

        new AzkarraApplication()
                .setContext(azkarraContext)
                .enableHttpServer(true, HttpServerConf.with("localhost", 8080))
                .run(args);
    }

    @NotNull
    private static Supplier<TopologyProvider> orderWorkflowTopologyProvider(Conf conf) {
        return () -> new OrderWorkflowStepTopologyProvider(() -> new OrderWorkflowTrackingTransformer(conf),
                topologyContext(conf));
    }

    private static OrderWorkflowTopologyContext topologyContext(Conf conf) {
        return new OrderWorkflowTopologyContext(
                streamTopicFor(conf, "topic.orderWorkflowStepCompleted"),
                streamTopicFor(conf, "topic.orderExpired"),
                streamTopicFor(conf, "topic.orderWorkflowStepsStore"),
                streamTopicFor(conf, "topic.orderShipped"),
                streamTopicFor(conf, "topic.orderInvoiced"),
                streamTopicFor(conf, "topic.orderCompleted"),
                streamTopicFor(conf, "topic.orderWorkflowStepsStore")
        );
    }

    private static <K extends SpecificRecord, V extends SpecificRecord> StreamTopic<K, V> streamTopicFor(Conf conf, String topicConfName) {
        return new StreamTopic<>(conf.getString(topicConfName), avroKeySerde(conf), avroValueSerde(conf));
    }
}
