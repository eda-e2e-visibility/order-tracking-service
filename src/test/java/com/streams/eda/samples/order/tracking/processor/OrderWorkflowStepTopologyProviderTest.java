package com.streams.eda.samples.order.tracking.processor;

import com.streams.eda.samples.order.tracking.util.StreamTopic;
import com.streams.eda.samples.utils.MockInMemorySerde;
import com.streams.samples.*;
import io.streamthoughts.azkarra.api.config.Conf;
import io.streamthoughts.azkarra.streams.config.AzkarraConf;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.processor.MockProcessorContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("unchecked")
class OrderWorkflowStepTopologyProviderTest {
    private OrderWorkflowStepTopologyProvider target;
    private MockProcessorContext processorContext;
    private Conf conf;
    private TopologyTestDriver topologyTestDriver;
    private TestInputTopic<OrderKey, OrderWorkflowStepCompleted> workflowStepCompletedTestInputTopic;
    private TestInputTopic<OrderKey, OrderShipped> orderShippedInputTopic;
    private TestInputTopic<InvoiceKey, OrderInvoiced> orderInvoicedInputTopic;
    private TestOutputTopic<OrderKey, OrderCompleted> orderCompletedOutputTopic;
    private TestOutputTopic<OrderKey, OrderExpired> expiredOrderTestOutputTopic;
    private TestInputTopic<OrderKey, OrderExpired> expiredOrderInputTopic;
    private OrderWorkflowTopologyContext topologyContext;
    private OrderWorkflowTrackingTransformer workflowStepTrackingTransformer;

    @BeforeEach
    void setUp() {
        conf = AzkarraConf.create("application.conf");
        topologyContext = initTopologyContext();
        var streamsProperties = conf.getSubConf("streams").getConfAsProperties();
        processorContext = new MockProcessorContext(streamsProperties);
        workflowStepTrackingTransformer = new OrderWorkflowTrackingTransformer(conf);
        workflowStepTrackingTransformer.init(processorContext);
        target = new OrderWorkflowStepTopologyProvider(() -> workflowStepTrackingTransformer, topologyContext);
        target.configure(conf);

        var topology = target.get();

        topologyTestDriver = new TopologyTestDriver(topology, streamsProperties);
        initTestInputOutputTopics();
    }

    @AfterEach
    void tearDown() {
        MockInMemorySerde.resetCache();
        closeTopologyTestDriver(topologyTestDriver);
    }

    @Test
    void shouldAddStateStoreToTopology() {
        assertThat(topologyTestDriver.getKeyValueStore(topologyContext.getOrderWorkflowTrackingStoreTopic().getName())).isNotNull();
    }

    @Test
    void givenNewOrderShouldAddEntryInStateStore() {
        var key = new OrderKey(UUID.randomUUID().toString());
        long timestamp = Instant.now().toEpochMilli();
        var value = orderPlacedWorkflowStep();
        processorContext.setTimestamp(timestamp);

        workflowStepCompletedTestInputTopic.pipeInput(key, value, timestamp);


        OrderWorkflowTrackingContainer storeEntry = getEntryFromStoreWithKey(key);
        assertThat(storeEntry.getLastCompletedWorkflowStep()).isEqualTo(value);
        assertThat(storeEntry.getOrderTime()).isEqualTo(processorContext.timestamp());
        assertThat(storeEntry.getTimeOfLastCompletedWorkflowStep()).isEqualTo(processorContext.timestamp());
    }


    @Test
    void shouldUpdateStateStoreWithLatestWorkflowStep() {
        var key = new OrderKey(UUID.randomUUID().toString());
        long timestampOfFirstRecord = Instant.now().toEpochMilli();
        long timestampOfSecondRecord = Instant.now().toEpochMilli();
        var value_1 = workflowStepFor(timestampOfFirstRecord, "order-placed", null);
        var value_2 = workflowStepFor(timestampOfSecondRecord, "order-shipped", "order-placed");

        workflowStepCompletedTestInputTopic.pipeInput(key, value_1, timestampOfFirstRecord);
        workflowStepCompletedTestInputTopic.pipeInput(key, value_2, timestampOfSecondRecord);

        OrderWorkflowTrackingContainer storeEntry = getEntryFromStoreWithKey(key);
        assertThat(storeEntry.getLastCompletedWorkflowStep()).isEqualTo(value_2);
        assertThat(storeEntry.getTimeOfLastCompletedWorkflowStep()).isEqualTo(timestampOfSecondRecord);
    }

    @Test
    void shouldPublishExpiredOrderEventAndRemoveEntryFromStateStoreWhenOrderIsExpired() {
        var key = new OrderKey(UUID.randomUUID().toString());
        long timestamp = Instant.now().toEpochMilli();
        var value = orderPlacedWorkflowStep();
        var placedOrderExpiryDuration = conf.getLong("app.workflowStepExpiryInMS.order-placed");
        //I had to add extra 1000 to make sure the punctuater works with the value greater than the expiry period
        //when doing advanceWallClockTime
        var advanceDuration = Duration.of(placedOrderExpiryDuration + 1000, ChronoUnit.MILLIS);

        workflowStepCompletedTestInputTopic.pipeInput(key, value, timestamp);
        topologyTestDriver.advanceWallClockTime(advanceDuration);

        assertThat(expiredOrderTestOutputTopic.readKeyValue().key).isEqualTo(key);
        assertThat(getEntryFromStoreWithKey(key)).isNull();
        assertThat(expiredOrderTestOutputTopic.isEmpty()).isTrue();
    }

    @Test
    void givenOrderIsShippedAndInvoicedOrderShouldBeCompletedAndRemovedFromInFlightStore() {
        var orderKey = new OrderKey(UUID.randomUUID().toString());
        var invoiceKey = new InvoiceKey(UUID.randomUUID().toString());
        long currentTime = Instant.now().toEpochMilli();
        var orderShipped = new OrderShipped(UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                currentTime, Collections.emptyList());
        var orderInvoiced = new OrderInvoiced(orderKey, currentTime, "100");

        orderShippedInputTopic.pipeInput(orderKey, orderShipped);
        orderInvoicedInputTopic.pipeInput(invoiceKey, orderInvoiced);

        var result = orderCompletedOutputTopic.readKeyValue();
        assertThat(result.key).isEqualTo(orderKey);
        assertThat(getEntryFromStoreWithKey(orderKey)).isNull();
        assertThat(orderCompletedOutputTopic.isEmpty()).isTrue();
    }

    @Test
    void givenOrderIsShippedAndInvoicedOrderShouldNotBeCompletedWhenItWasExpired() {
        var orderKey = new OrderKey(UUID.randomUUID().toString());
        var invoiceKey = new InvoiceKey(UUID.randomUUID().toString());
        var currentTime = Instant.now().toEpochMilli();
        var randomUUID = UUID.randomUUID().toString();
        var orderShipped = new OrderShipped(randomUUID, randomUUID, currentTime, Collections.emptyList());
        var orderInvoiced = new OrderInvoiced(orderKey, currentTime, "100");
        var orderExpired = new OrderExpired(currentTime, orderPlacedWorkflowStep());

        orderShippedInputTopic.pipeInput(orderKey, orderShipped);
        //Make sure order-expired event is published before one of the joined events so the topology can load
        //the expired-orders in to the KTable for lookup to occur
        expiredOrderInputTopic.pipeInput(orderKey, orderExpired);
        orderInvoicedInputTopic.pipeInput(invoiceKey, orderInvoiced);

        assertThat(orderCompletedOutputTopic.isEmpty()).isTrue();
        assertThat(getEntryFromStoreWithKey(orderKey)).isNull();
    }

    private OrderWorkflowStepCompleted orderPlacedWorkflowStep() {
        return new OrderWorkflowStepCompleted("order-placed", null, Instant.now().toEpochMilli());
    }

    private OrderWorkflowStepCompleted workflowStepFor(long currentTime, String s, String o) {
        return new OrderWorkflowStepCompleted(s, o, currentTime);
    }

    private void initTestInputOutputTopics() {
        var workflowStepCompletedTopic = topologyContext.getWorkflowStepCompletedTopic();
        var expiredOrderTopic = topologyContext.getExpiredOrderTopic();
        var shippedOrderTopic = topologyContext.getShippedOrderTopic();
        var invoicedOrderTopic = topologyContext.getInvoicedOrderTopic();
        var completedOrderTopic = topologyContext.getCompletedOrderTopic();

        workflowStepCompletedTestInputTopic = topologyTestDriver.createInputTopic(
                workflowStepCompletedTopic.getName(),
                workflowStepCompletedTopic.getKeySerde().serializer(), workflowStepCompletedTopic.getValueSerde().serializer());

        orderShippedInputTopic = topologyTestDriver.createInputTopic(shippedOrderTopic.getName(),
                shippedOrderTopic.getKeySerde().serializer(), shippedOrderTopic.getValueSerde().serializer());

        orderInvoicedInputTopic = topologyTestDriver.createInputTopic(invoicedOrderTopic.getName(),
                invoicedOrderTopic.getKeySerde().serializer(), invoicedOrderTopic.getValueSerde().serializer());

        expiredOrderInputTopic = topologyTestDriver.createInputTopic(expiredOrderTopic.getName(),
                expiredOrderTopic.getKeySerde().serializer(), expiredOrderTopic.getValueSerde().serializer());

        orderCompletedOutputTopic = topologyTestDriver.createOutputTopic(completedOrderTopic.getName(),
                completedOrderTopic.getKeySerde().deserializer(), completedOrderTopic.getValueSerde().deserializer());

        expiredOrderTestOutputTopic = topologyTestDriver.createOutputTopic(expiredOrderTopic.getName(),
                expiredOrderTopic.getKeySerde().deserializer(), expiredOrderTopic.getValueSerde().deserializer());
    }

    private OrderWorkflowTopologyContext initTopologyContext() {
        return new OrderWorkflowTopologyContext(
                streamTopicWithMockSerdeFor("topic.orderWorkflowStepCompleted"),
                streamTopicWithMockSerdeFor("topic.orderExpired"),
                streamTopicWithMockSerdeFor("topic.orderWorkflowStepsStore"),
                streamTopicWithMockSerdeFor("topic.orderShipped"),
                streamTopicWithMockSerdeFor("topic.orderInvoiced"),
                streamTopicWithMockSerdeFor("topic.orderCompleted"),
                streamTopicWithMockSerdeFor("topic.orderWorkflowStepsStore")
        );
    }

    private <K, V> StreamTopic<K, V> streamTopicWithMockSerdeFor(String topicConfName) {
        return new StreamTopic<>(conf.getString(topicConfName), new MockInMemorySerde<>(), new MockInMemorySerde<>());
    }

    private OrderWorkflowTrackingContainer getEntryFromStoreWithKey(OrderKey key) {
        return topologyTestDriver.<OrderKey, OrderWorkflowTrackingContainer>getKeyValueStore(topologyContext.getOrderWorkflowTrackingStoreTopic().getName())
                .get(key);
    }

    private void closeTopologyTestDriver(TopologyTestDriver topologyTestDriver) {
        try {
            topologyTestDriver.close();
        } catch (final RuntimeException e) {
            // https://issues.apache.org/jira/browse/KAFKA-6647 causes exception when executed in Windows, ignoring it
            // Logged stacktrace cannot be avoided
            System.out.println("Ignoring exception, test failing in Windows due this exception:" + e.getLocalizedMessage());
        }
    }
}