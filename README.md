# Order tracking service

A service to track the end-to-end business process of orders. It is responsible for the following

* Track stuck orders in any of the workflow steps.
* Mark orders as completed when they finish the workflow end-to-end. 

### Order workflow

The following shows the lifecycle that order went through in all order workflow services.

<p align="center">
  <img  src="docs/order-workflow.png?raw=true" title="Order workflow">
</p>

### Expiring orders

The order-tracking service is tracking in-flight orders by consuming `order-workflow-step-completed` events.
At the moment it stores only the last step the order went through in a state store, so when order is stuck after this completed step, 
the order-tracking service will expire this order by publishing `order-expired` event and remove it from the order in-flight state store.
The SLA to expire order in each workflow step is configurable using properties in `app.workflowStepExpiryInMS` 
(configs can be found in [application.conf](src/main/resources/application.conf)).
The order tracking service is running a punctuator to check for those stuck in-flight orders.
Orders gets removed from this in-flight store in one of the following cases

* Order is completed. This happens when a `order-workflow-step-completed` is received with step name `order-completed`. 
(I chose this approach instead of consuming from `order-completed` to make it easier in the sample code).
* Order is expired. When the order-tracking service decides to expire an order, it removes it from the store to avoid publishing multiple expired events for the same order.

### Order Completed

As shown in the previous diagram, order is considered to be completed only when
`order-invoiced` and `order-shipped` are published for that order. 
One exception to this rule is the expired orders. Lets for example assume that order was stuck in shipping service for some time, so the order-tracking service decided to
expire this order by publishing `order-expired`. Later, the shipping service finished and published a `order-shipped` event for this order.
In that case, we do not want the order to be marked as completed.
As a result, we filter out any expired order before publishing the completed ones as shown in this code snippet.

``` java
var expiredOrdersTable = this.tableFor(streamsBuilder, topologyContext.getExpiredOrderTopic());
var orderCompletedStream = filterOutExpiredOrders(toCompletedOrder(orderShippedStream, orderInvoicedStream), expiredOrdersTable);

orderCompletedStream.to(topologyContext.getCompletedOrderTopic().getName());
```  

